use argh::FromArgs;
use std::path::PathBuf;

mod crontab;

#[derive(Debug, PartialEq, FromArgs)]
#[argh(subcommand)]
enum CronType {
    Tab(TabArgs),
    File(FileArgs),
}

#[derive(FromArgs, PartialEq, Debug)]
/// Cron with a smile ☺
struct CronArgs {
    /// type of cron to look at
    #[argh(subcommand)]
    cron_type: CronType,
}

#[derive(FromArgs, PartialEq, Debug)]
#[argh(subcommand, name = "file")]
/// introspect cron file
struct FileArgs {
    /// cron file apth
    #[argh(option)]
    path: PathBuf,

    /// whether to invoke editing or not
    #[argh(switch, short = 'e')]
    edit: bool,
}

#[derive(FromArgs, PartialEq, Debug)]
#[argh(subcommand, name = "tab")]
/// introspect crontab
struct TabArgs {
    /// whether to invoke editing or not
    #[argh(switch, short = 'e')]
    edit: bool,
}

fn main() {
    let args: CronArgs = argh::from_env();
    match args.cron_type {
        CronType::Tab(args) => crontab::init(args.edit),
        CronType::File(args) => (),
    };
}
