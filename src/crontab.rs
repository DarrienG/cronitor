mod parser;

pub fn init(edit: bool) {
    parser::parse(edit);
}
