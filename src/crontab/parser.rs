use cron::Schedule;
use std::{io, process::Command, str::FromStr};
use thiserror::Error;

#[derive(PartialEq, Debug)]
pub struct CrontabExpression {
    schedule: Schedule,
    command: String,
}

#[derive(Error, Debug)]
pub enum ParseError {
    #[error("Failed to parse expression: \n\tline number:{line_number:?}\n\t{failed_line:?}")]
    ParseFail {
        line_number: usize,
        failed_line: String,
    },
    #[error("Failed to execute crontab: {0}")]
    ExecuteError(io::Error),
}

#[derive(PartialEq, Debug)]
pub enum ParsedLine {
    // Crontab line with nothing worth rendering
    NoOp(String),
    Expression(CrontabExpression),
}

// TODO: we'll take this edit flag, but pretend it's read only for MVP
// right now we're splitting on spaces, we should instead get the indexes of where everything is
// so we preserve user formatting when allowing editing
pub fn parse(edit: bool) -> Result<Vec<ParsedLine>, ParseError> {
    match Command::new("crontab").arg("-l").output() {
        Ok(output) => parse_crontab(&String::from_utf8_lossy(&output.stdout)),
        Err(e) => Err(ParseError::ExecuteError(e)),
    }
}

fn parse_crontab(output: &str) -> Result<Vec<ParsedLine>, ParseError> {
    let mut values: Vec<ParsedLine> = Vec::new();
    for (index, line) in output.split("\n").enumerate() {
        match line.trim().chars().nth(0) {
            None => values.push(ParsedLine::NoOp("".to_owned())),
            Some(start_char) => {
                if start_char == '#' {
                    values.push(ParsedLine::NoOp(line.to_owned()))
                } else {
                    let parse_result = parse_line(line, index);
                    if parse_result.is_err() {
                        return Err(parse_result.unwrap_err());
                    }
                    values.push(parse_result.unwrap());
                }
            }
        }
    }
    Ok(values)
}

fn parse_line(line: &str, line_number: usize) -> Result<ParsedLine, ParseError> {
    let split = split_on_spaces(line);

    if split.len() < 5 {
        return Err(ParseError::ParseFail {
            line_number,
            failed_line: line.to_owned(),
        });
    }

    let cron_expr = split[0..5].join(" ");

    // cron parser uses nonstandard second and year format
    // required extra allocation to make sure we hit this :(
    match Schedule::from_str(&format!("{} {} {}", "*", cron_expr, "*")) {
        // invalid cron expression received from user
        Err(_) => Err(ParseError::ParseFail {
            line_number,
            failed_line: line.to_owned(),
        }),
        // line is good, it's go time
        Ok(schedule) => Ok(ParsedLine::Expression(CrontabExpression {
            schedule,
            command: split[5..].join(" "),
        })),
    }
}

// This could be done with a regex, let's find one later
fn split_on_spaces(line: &str) -> Vec<String> {
    let line = line.trim();
    let mut tokens = Vec::new();

    let mut current_word: Vec<char> = Vec::new();

    for character in line.chars() {
        if character == ' ' || character == '\t' {
            if !current_word.is_empty() {
                tokens.push(current_word.iter().collect::<String>());
                current_word = Vec::new();
            }
        } else {
            current_word.push(character);
        }
    }

    if !current_word.is_empty() {
        tokens.push(current_word.iter().collect::<String>());
    }

    tokens
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn simple_parse() {
        let cron_expression = r#"# this is a test, will we skip this line?

* * * * * ls -al *.txt"#;
        let parse_result = parse_crontab(cron_expression);
        assert!(parse_result.is_ok());

        let parse_result = parse_result.unwrap();

        let expected = vec![
            ParsedLine::NoOp("# this is a test, will we skip this line?".to_owned()),
            ParsedLine::NoOp("".to_owned()),
            ParsedLine::Expression(CrontabExpression {
                schedule: Schedule::from_str("* * * * * * *").unwrap(),
                command: "ls -al *.txt".to_owned(),
            }),
        ];

        assert_eq!(&parse_result, &expected);
    }

    #[test]
    fn complex_parse() {
        let cron_expression = r#"# this is a test, will we skip this line?

30 9,12,15 1,15 May-Aug Mon,Wed,Fri grep -U $'\015' curl1.txt"#;
        let parse_result = parse_crontab(cron_expression);
        assert!(parse_result.is_ok());

        let parse_result = parse_result.unwrap();

        let expected = vec![
            ParsedLine::NoOp("# this is a test, will we skip this line?".to_owned()),
            ParsedLine::NoOp("".to_owned()),
            ParsedLine::Expression(CrontabExpression {
                schedule: Schedule::from_str("* 30 9,12,15 1,15 May-Aug Mon,Wed,Fri *").unwrap(),
                command: r#"grep -U $'\015' curl1.txt"#.to_owned(),
            }),
        ];

        assert_eq!(&parse_result, &expected);
    }

    #[test]
    fn multi_expression_parse() {
        let cron_expression = r#"# * * * * * ls -al *.txt ignore this comment
1 2 3 4 5 git status
5 4 * * * cargo build /tmp/my-project
30 9,12,15 1,15 May-Aug Mon,Wed,Fri grep -U $'\015' curl1.txt"#;
        let parse_result = parse_crontab(cron_expression);
        assert!(parse_result.is_ok());

        let parse_result = parse_result.unwrap();

        let expected = vec![
            ParsedLine::NoOp("# * * * * * ls -al *.txt ignore this comment".to_owned()),
            ParsedLine::Expression(CrontabExpression {
                schedule: Schedule::from_str("* 1 2 3 4 5 *").unwrap(),
                command: r#"git status"#.to_owned(),
            }),
            ParsedLine::Expression(CrontabExpression {
                schedule: Schedule::from_str("* 5 4 * * * *").unwrap(),
                command: r#"cargo build /tmp/my-project"#.to_owned(),
            }),
            ParsedLine::Expression(CrontabExpression {
                schedule: Schedule::from_str("* 30 9,12,15 1,15 May-Aug Mon,Wed,Fri *").unwrap(),
                command: r#"grep -U $'\015' curl1.txt"#.to_owned(),
            }),
        ];

        assert_eq!(&parse_result, &expected);
    }

    #[test]
    fn test_splitter_works() {
        let split = split_on_spaces("   hey there        buddy ");
        assert_eq!(split, vec!["hey", "there", "buddy"]);

        let split = split_on_spaces("                ");
        let expected: Vec<String> = Vec::new();
        assert_eq!(split, expected);
    }
}
