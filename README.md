# Cronitor

Cron with a smile ☺

Cronitor is your all in one solution for looking at cron files. Whether you're
using crontab, anacron, or just some random cron file it does it all.

## Development in progress

Note this project is under development and does not really do a whole lot right
now. Give it a few weeks though :)

## Cron, but all in one place

**ADD SCREENSHOT HERE!!!**

Get all of your cron in one place. Logs, next run time, whether the last run was
a success, and more! Fire up cronitor and it will analyze your cron files with
surprising speed.

Cronitor has two modes: tab (for crontab) and file (for opening anacron files
and random cron files).

### Crontab mode

In crontab mode, all you have to do is run: `cronitor tab` and it will
automatically open the crontab file for your user. Everything is editable too if
run with -e :)

Crontab mode also works when getting data from stdin! Set it as your EDITOR and
go to town if you would prefer to still invoke crontab. This will provide a safe
visudo like environment for editing cron files by default.

### Cron file mode

In cron file mode, you can point crontab at a file and it will figure out where
to go from there. `cronitor file /my/cron/file`

If pointed at a file in cron.daily, cron.weekly, or any other anacron folder,
it will assume you are working with an anacron folder.h

If pointed anywhere else, it will enter standard cron mode and assume your file
is just a standard crontab file, providing much of the same information crontab
uses.

# Feature work progress

- [ ] Crontab mode
   - [ ] Parsing cron files
   - [ ] Determining when last run was
   - [ ] Determining when cron will run next
   - [ ] Figuring out where cron log is per command
   - [ ] Provide edit mode
   - [ ] Describe cron command in english (e.g. crontab.guru)
   - [ ] Run cron with proper cron env
- [ ] Cron file mode
  - [ ] anacron
     - [ ] Determine when last run was
     - [ ] Guestimating when next run will be
     - [ ] Figuring out where cron log is per command
     - [ ] Provide edit mode
     - [ ] Run cron with proper cron env
   - [ ] free file
      - [ ] Determining when cron would run next
      - [ ] Provide edit mode
      - [ ] Describe cron command in english (e.g. crontab.guru)

